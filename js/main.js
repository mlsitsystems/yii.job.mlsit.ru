$(function(){

	/**
	 *	Добавление и удаление строк таблиц основной формы
	 */
	var allowedRows = 3;
	$('.extra-row > td:first-child > div > div').click(function(){
		var tbody = $(this).parents('tbody');
		var countRows = tbody.find('tr:not(.extra-row)').length;
		var newRow = tbody.find('tr:first-child').clone();
		//var newNameAttr = newRow.find('input, textarea').val('').attr('name').replace(/\[[0-9]+\]/,'['+countRows+']');
		newRow.find('input, textarea').val('').attr('name',function(i,attr){
			return attr.replace(/\[[0-9]+\]/,'['+countRows+']');
		});
		newRow.find('.error').removeClass('error');
		newRow.insertBefore(tbody.find('tr.extra-row'));
		tbody.find('.delete-row').remove();
		newRow.find('td:last-child div').append('<img src="'+homeUrl+'img/icon-minus.png" class="delete-row" title="Удалить строку">'); // homeUrl определена глобально в <head>
		if (countRows+1 >= allowedRows)
			tbody.find('tr.extra-row').hide();
	});
	$(document).on('click','.delete-row',function(){
		var tbody = $(this).parents('tbody');
		var countRows = tbody.find('tr:not(.extra-row)').length-1;
		tbody.find('tr.extra-row').show();
		if(countRows>1)
			tbody.find('tr:nth-child('+countRows+') td:last-child div').append('<img src="'+homeUrl+'img/icon-minus.png" class="delete-row" title="Удалить строку">');
		$(this).parents('tr').remove();
	});
	
	/**
	 *	Слайдеры для полей selfratingN
	 */
	var selects = $('.selfrating-select').hide();
	$.each(selects,function(i, el){		
		$('<div id="sliderSelfrating'+(i+1)+'"></div>').insertAfter(el).slider({
			animate: true,
			min: 0,
			max: 10,
			range: 'min',
			value: $(this).parents('.slider-group').find('select')[0].selectedIndex,
			slide: function(event, ui) {
				$(this).parents('.slider-group').find('select')[0].selectedIndex = ui.value;
				$(this).parents('.slider-group').find('.slider-value-inner').html(ui.value);
			},
			create: function(){
				var a = $(this).parents('.slider-group').find('select')[0].selectedIndex
				$(this).parents('.slider-group').find('.slider-value-inner').html(a);
			}
		});
	});
	
});