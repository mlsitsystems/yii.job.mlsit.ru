drop table if exists tbl_applicants;
create table tbl_applicants (
	id integer not null primary key autoincrement,
	vacancy integer not null references tbl_vacancies(id) on update cascade on delete cascade,
	fullname text not null,
	datebirth text,
	addrreal text,
	addrreg text,
	phones text not null,
	selfrating1 integer,
	selfrating2 integer,
	selfrating3 integer,
	selfrating4 integer,
	selfrating5 integer,
	pc text,
	driving text,
	car integer,	
	lang text,
	family text,
	children integer,
	hobby text,
	medrestr text,
	credit text,
	outlaw text,
	salarywish text,
	skills text,
	achievements text,
	reasonprof text,
	readylongwork integer,
	readytrip text,
	datework text,
	army text,
	badhabits text,
	passpseries integer,
	passpnum integer,
	passpgivenby text
);

drop table if exists tbl_applicant_education;
create table tbl_applicant_education (
	id integer not null primary key autoincrement,
	applicant_id integer references tbl_applicants(id) on update restrict on delete cascade,
	yearstart text,
	yearend text,
	name text,
	faculty text,
	speciality text,
	qualification text
);

drop table if exists tbl_applicant_courses;
create table tbl_applicant_courses (
	id integer not null primary key autoincrement,
	applicant_id integer references tbl_applicants(id) on update restrict on delete cascade,
	timeperiod text,
	description text
);

drop table if exists tbl_applicant_profession;
create table tbl_applicant_profession (
	id integer not null primary key autoincrement,
	applicant_id integer references tbl_applicants(id) on update restrict on delete cascade,
	namedesc text,
	timeperiod text,
	position text,
	salary text,
	duties text,
	reasondism text
);

drop table if exists tbl_vacancies;
create table tbl_vacancies (
	id integer not null primary key autoincrement,
	title text not null
);
