<?php

class DefaultController extends Controller
{
	public $defaultAction='dashboard';
	
	public function filters() {
		return array(
			'accessControl',
		);
	}
	
	public function accessRules() {
		return array(
			array(
				'allow',
				'users'=>array('@'),
			),
			array('deny'),
		);
	}
	
	public function actionDashboard() {
		$this->layout='main';
		$this->render('dashboard',array('assetsUrl'=>$this->module->params->assetsUrl));
	}
	
	public function actionApplicants($do=null,$id=null) {
		$this->layout='main';
		switch($do){
			case 'view':
				$this->renderText('Просмотр анкеты №'.$id);
				break;
			case 'delete':
				$this->renderText('Удаление анкеты №'.$id);
				break;
			default:
				$applicants = new CActiveDataProvider('Applicants',array(
					'criteria'=>array(
						'select'=>array(
							'id',
							'fullname',
							'vacancy',
						),
					),
				));
				$this->render('applicants',array('applicants'=>$applicants));
		}
	}
	
	public function actionVacancies($do=null,$id=null) {
		$this->asideMenu = array(
			array('label'=>'Добавить вакансию','url'=>array('/admin/default/vacancies','do'=>'create')),
		);
		switch($do){
			case 'create':
				$this->breadcrumbs = array(
					'Панель управления'=>array('/admin'),
					'Открытые вакансии'=>array('/admin/default/vacancies'),
					'Добавление вакансии'
				);
				$model=new Vacancies;
				if(isset($_POST['Vacancy'])) {
					$model->attributes=$_POST['Vacancy'];
					if($model->save())
						$this->redirect(array('/admin/vacancies'));
				}
				$this->render('vacancyForm',array('model'=>$model));
				break;
			case 'update':
				$this->breadcrumbs = array(
					'Панель управления'=>array('/admin'),
					'Открытые вакансии'=>array('/admin/default/vacancies'),
					'Обновление вакансии'
				);
				$model=Vacancies::model()->findByPk($id);
				if($model===null) throw new CHttpException(404,'The requested page does not exist.');
				if(isset($_POST['Vacancy'])) {
					$model->attributes=$_POST['Vacancy'];
					if($model->save())
						$this->redirect(array('/admin/vacancies'));
				}
				$this->render('vacancyForm',array('model'=>$model));
				break;
			case 'delete':
				Vacancies::model()->findByPk($id)->delete();
				$this->redirect(array('/admin/vacancies'));
				break;
			default:
				$this->breadcrumbs = array(
					'Панель управления'=>array('/admin'),
					'Открытые вакансии',
				);
				$vacancies = new CActiveDataProvider('Vacancies');
				$this->render('vacancies',array('vacancies'=>$vacancies));
		}
	}
	
	public function actionSettings() {
		$settingsFile=$this->module->basePath.DIRECTORY_SEPARATOR.$this->module->params->settingsFile;
		$model=new SettingsForm;
		if(isset($_POST['SettingsForm'])) {
			$model->attributes=$_POST['SettingsForm'];
			if($model->validate()) {
				$fp=@fopen($settingsFile,'w');
				@flock($fp,LOCK_EX);
				@fwrite($fp,CJSON::encode($model->attributes));
				@flock($fp,LOCK_UN);
				@fclose($fp);
				Yii::app()->user->setFlash('settingsValidationSuccess','Настройки сохранены.');
				$this->refresh();
			} else {				
				Yii::app()->user->setFlash('settingsValidationError','Валидация не прошла.');
			}
		} else {
			$model->attributes=CJSON::decode(file_get_contents($settingsFile,true));
		}
		$this->render('settings',array('model'=>$model));
	}
}