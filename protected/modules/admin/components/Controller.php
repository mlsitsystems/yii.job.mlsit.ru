<?php
class Controller extends CController {
	private $_asideMenu=array();
	private $_breadcrumbs=array();
	
	public function getAsideMenu() {
		if (!empty($this->_asideMenu)) 
			return $this->widget('admin.components.MenuBlock', array('title'=>'Действия','items'=>$this->_asideMenu), true);
		return '';
	}
	
	public function setAsideMenu($value) {
		$this->_asideMenu=$value;
	}
	
	public function getBreadcrumbs() {
		if($this->module->params->breadcrumbsEnabled && !empty($this->_breadcrumbs)) {
			$this->_breadcrumbs = array_merge(array("<img src='".$this->module->params->faviconUrl."' title='Перейти на сайт'>"=>Yii::app()->homeUrl),$this->_breadcrumbs);
			return $this->widget('zii.widgets.CBreadcrumbs', array('homeLink'=>false,'links'=>$this->_breadcrumbs,'separator'=>' › ','encodeLabel'=>false), true);
		}
		return '';
	}
	
	public function setBreadcrumbs($value) {
		$this->_breadcrumbs=$value;
	}
}
?>