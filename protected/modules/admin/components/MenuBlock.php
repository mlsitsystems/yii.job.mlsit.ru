<?php
class MenuBlock extends CWidget {
	public $title;
	public $visible=true;
	public $items;
	
	public function init() {}
	
	public function run() {
		if ($this->visible && !empty($this->items)) {
			$this->render('menuBlock',array('uniqueId'=>$this->getId(),'title'=>$this->title,'items'=>$this->items));
		}
	}
}
?>