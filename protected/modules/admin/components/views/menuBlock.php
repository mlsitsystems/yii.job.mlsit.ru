<div id="<?php echo $uniqueId; ?>" class="menublock">
	<?php if (!empty($title)): ?>
		<h4 class="menublock-title"><?php echo $title ?></h4>
	<?php endif; ?>
	<?php $this->widget('zii.widgets.CMenu', array('items'=>$items)); ?>
</div>
