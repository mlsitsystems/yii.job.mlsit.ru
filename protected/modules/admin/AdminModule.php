<?php

class AdminModule extends CWebModule
{
	public $layout = 'main-aside';
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		$this->params->settingsFile='settings.json';
		$this->params->name='Панель управления';
		$this->params->breadcrumbsEnabled=false;
		$this->params->homeUrl=Yii::app()->homeUrl.$this->id;
		$this->params->navMenu=array(
			array('label'=>'Анкеты','url'=>array('/admin/default/applicants')),
			array('label'=>'Вакансии','url'=>array('/admin/default/vacancies')),
			array('label'=>'Настройки','url'=>array('/admin/default/settings')),
		);
		$this->params->assetsUrl=Yii::app()->assetManager->publish($this->basePath.'/assets',false,-1,true);
		if (file_exists(dirname(Yii::app()->request->scriptFile).DIRECTORY_SEPARATOR.'favicon.ico'))
			$this->params->faviconUrl = Yii::app()->homeUrl.'favicon.ico';
		else
			$this->params->faviconUrl = $this->params->assetsUrl.'/adminicon.ico';
		
		$config=CJSON::decode(file_get_contents($this->basePath.DIRECTORY_SEPARATOR.$this->params->settingsFile),true);
		$this->params=CMap::mergeArray($this->params,$config);

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
