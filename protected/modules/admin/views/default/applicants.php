<?php
	$this->breadcrumbs = array(
		$this->module->params->name => array('/admin'),
		'Заполненные анкеты',
	);
	$this->asideMenu = array(
		array('label'=>'Создать анкету','url'=>array('/admin/applicants','id'=>2)),
	);
?>
<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$applicants,
		'columns'=>array(
			'id',
			'fullname',
			'vacancy',
			array(
				'class'=>'CLinkColumn',
				'label'=>'Подробнее',
				'urlExpression'=>'Yii::app()->controller->createUrl("default/applicants",array("do"=>"view","id"=>$data->id))',
			),
		),
		'htmlOptions'=>array(
			'style'=>'padding:0;',
		),
		'template'=>'{items}{pager}',
		'enableSorting'=>false,
		'selectableRows'=>0,
	));
?>
