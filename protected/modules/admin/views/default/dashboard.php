<?php
$this->breadcrumbs=array(
	$this->module->params->name,
);
?>
<ul id="dashboardBlocks" class="cf">
	<li>
		<a href="<?php echo $this->createUrl('default/applicants'); ?>" class="thumbnail">
			<img src="<?php echo $assetsUrl ?>/img/icon-emails.png" class="dashboard-block-icon">
			<span class="dashboard-block-headline">Заполненные анкеты</span>
			<span class="dashboard-block-description">Список всех заполненных анкет</span>
		</a>
	</li>
	<li>
		<a href="<?php echo $this->createUrl('default/vacancies'); ?>" class="thumbnail">
			<img src="<?php echo $assetsUrl ?>/img/icon-vacancies.png" class="dashboard-block-icon">
			<span class="dashboard-block-headline">Открытые вакансии</span>
			<span class="dashboard-block-description">Добавление/удаление вакансий компании</span>
		</a>
	</li>
	<!--
	-->
	<li>
		<a href="<?php echo $this->createUrl('default/settings'); ?>" class="thumbnail">
			<img src="<?php echo $assetsUrl ?>/img/icon-tools.png" class="dashboard-block-icon">
			<span class="dashboard-block-headline">Настройки</span>
			<span class="dashboard-block-description">Настройки приложения</span>
		</a>
	</li>
</ul>
