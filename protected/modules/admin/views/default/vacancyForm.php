<?php
/* @var $this VacanciesController */
/* @var $model Vacancies */
/* @var $form CActiveForm */
?>

<div class="flash"><?php echo Yii::app()->user->getFlash('vacancySaveErrors') ?></div>
<form id="vacancyForm" method="post" action="">
	<p>
		<label for="inputVacancyTitle">Название вакансии:</label>
		<input type="text" id="inputVacancyTitle" name="Vacancy[title]" value="<?php echo $model->title ?>" style="width:250px;font-size:12px;">
	</p>
	<p>
		<button type="submit" class="btn btn-big btn-round">Создать</button>
	</p>
</form>
