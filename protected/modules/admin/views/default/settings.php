<?php echo Yii::app()->user->getFlash('settingsValidationSuccess'); ?>
<?php echo Yii::app()->user->getFlash('settingsValidationError'); ?>
<form id="settingsForm" method="post" action="">
	<p>
		<label>Навигационная цепочка:</label>
		<?php
			echo CHtml::activeRadioButtonList(
				$model,
				'breadcrumbsEnabled',
				array(
					1=>'Включена',
					0=>'Выключена',
				)
			);
		?>
		<!--
		<label for="inputBreadcrumbsEnabled"><input type="radio" id="inputBreadcrumbsEnabled" name="Settings[breadcrumbsEnabled]" value="0" checked> Включена</label>
		<label for="inputBreadcrumbsDisabled"><input type="radio" id="inputBreadcrumbsDisabled" name="Settings[breadcrumbsEnabled]" value="1"> Выключена</label>
		-->
	</p>
	
	<p><button type="submit" class="btn btn-big btn-round">Сохранить</button></p>
</form>