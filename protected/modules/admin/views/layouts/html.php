<?php
	$module = $this->module;
	$params = $module->params;
	$headTitle = $params->name.' – '.CHtml::encode($this->pageTitle);
	$navMenu = '';
	if (!empty($params->navMenu))
		$navMenu = $this->widget('zii.widgets.CMenu', array('items'=>$params->navMenu, 'id'=>'navMenu'), true);
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="<?php echo $params->faviconUrl ?>" type="image/x-icon">
		<link rel="stylesheet" href="<?php echo $params->assetsUrl ?>/css/bubble.css">
		<link rel="stylesheet" href="<?php echo $params->assetsUrl ?>/css/main.css">
		<link rel="stylesheet" href="<?php echo $params->assetsUrl ?>/fonts/opensans.css">
		<title><?php echo $headTitle ?></title>
		<!--[if lt IE 9]>
		<script src="<?php echo $params->assetsUrl ?>/js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<header>
				<nav class="cf">
					<div id="navBrand">
						<a href="<?php echo $params->homeUrl ?>"><?php echo $params->name ?></a>
					</div>
					<?php echo $navMenu ?>
					<ul id="navUserMenu">
						<li><a href="<?php echo Yii::app()->homeUrl ?>"><img src="<?php echo $params->assetsUrl ?>/img/icon-exit.png"> Перейти на сайт</a></li>
					</ul>
				</nav>
			</header>
			<div id="page" class="cf">
				<?php echo $content ?>
			</div>
		</div>
		<footer>
			<div class="inner"><table><tr><td>
				&copy; 2013<?php if(date('Y')!='2013') echo '-'.date('Y') ?> <?php echo Yii::app()->name ?>
			</td></tr></table></div>
		</footer>
		
		<script src="<?php echo $params->assetsUrl ?>/js/main.js"></script>
	</body>
</html>
