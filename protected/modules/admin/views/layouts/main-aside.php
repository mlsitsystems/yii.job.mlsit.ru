<?php $this->beginContent('/layouts/html') ?>
	<div id="main">
		<div class="inner">
			<?php echo $this->breadcrumbs ?>
			<?php echo $content ?>
		</div>
	</div>
	<aside>
		<?php echo $this->asideMenu ?>
	</aside>
<?php $this->endContent() ?>
