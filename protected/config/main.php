<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Анкета для соискателей - MLS IT Systems',
	'language'=>'ru',
	'preload'=>array(
		'log',
	),
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'layout'=>'//layouts/main',
	'modules'=>array(
		'admin',
		/*
		*/
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'pwd',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>false,
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				''=>'site/index',
				'success'=>'site/success',
				'admin/<action:\w+>'=>'admin/default/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			/*
			*/
		),
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/db.sqlite',
			'tablePrefix'=>'tbl_',
			'initSQLs'=>array(
				'PRAGMA foreign_keys=true',
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
		'bootstrap'=>array(
			'class'=>'ext.bootstrap.components.Bootstrap',
			'jqueryCss'=>false,
		),
	),

	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);