<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/front.css');
	$errorClass=" error";
	Yii::app()->clientScript->registerScript('tooltips','$(function(){$("input,select,textarea").tooltip({placement:"right"})});',CClientScript::POS_END);
	Yii::app()->clientScript->registerScript('datepicker','$(function(){$("#inputDatebirth").datepicker({changeMonth:true,changeYear:true,yearRange:"-65:-14",minDate:"-65y",maxDate:"-14y",dateFormat:"dd MM yy"})});',CClientScript::POS_END);
	Yii::app()->clientScript->registerScript('datepicker2','$(function(){$("#inputDatework").datepicker({dateFormat:"dd MM yy"})});',CClientScript::POS_END);
?>
<div id="afContainer">
	<img id="afCorner" src="<?php echo Yii::app()->homeUrl ?>img/corner.png">
	<img id="afCard" src="<?php echo Yii::app()->homeUrl ?>img/card.png">
	<form id="applicantForm" class="form-horizontal" action="<?php echo Yii::app()->homeUrl ?>" method="post">
		
		<h1 id="afTitle">А Н К Е Т А &nbsp; С О И С К А Т Е Л Я</h1>
		
		<p class="text-center form-row<?php if($applicants->hasErrors('vacancy')) echo $errorClass; ?>">
			<label for="inputVacancy" class="inline" style="margin-right:16px;"><em>на должность</em><i title="Это поле необходимо заполнить"></i></label>
			<?php 
			$vacs=Vacancies::model()->findAll();
			$selItems=array(''=>'Выберите из списка');
			foreach($vacs as $model){
				$selItems[$model->id]=$model->title;
			}
			/*
			$selItems=array(
				''=>'Выберите из списка',
				'0'=>'Менеджер по продажам',
				'1'=>'Программист Delphi',
				'2'=>'Программист PHP',
				'3'=>'Специалист технической поддержки'
			);
			*/
			//echo '<pre>'.CVarDumper::dumpAsString($selItems).'</pre>';
			echo CHtml::dropDownList('applicants[vacancy]',$applicants->vacancy,$selItems,array(
				'id'=>'inputVacancy',
				'data-toggle'=>'tooltip',
				'title'=>CHtml::encode($applicants->getError('vacancy')),
			)) ?>
		</p>
		
		<section style="margin:35px auto; width:463px;">
			<div class="control-group<?php if($applicants->hasErrors('fullname')) echo $errorClass; ?>" style="margin-top:30px;">
				<label for="inputFullname" class="control-label">Фамилия, имя, отчество<i title="Это поле необходимо заполнить"></i></label>
				<div class="controls">
					<input type="text" id="inputFullname" name="applicants[fullname]" value="<?php echo CHtml::resolveValue($applicants,'fullname') ?>" data-toggle="tooltip" title="<?php echo CHtml::encode($applicants->getError('fullname')) ?>">
				</div>
			</div>
			<div class="control-group">
				<label for="inputDatebirth" class="control-label">Дата рождения</label>
				<div class="controls" style="white-space:nowrap;">
					<input type="text" id="inputDatebirth" name="applicants[datebirth]" value="<?php echo CHtml::resolveValue($applicants,'datebirth') ?>" data-toggle="tooltip" title="<?php echo CHtml::encode($applicants->getError('datebirth')) ?>">
					<span style="position:relative;"><i class="icon-calendar" style="position:absolute; left:-21px; top:0; opacity:.7;" title="Ввод с помощью календаря"></i></span>
				</div>
			</div>
			<div class="control-group">
				<label for="inputAddrreal" class="control-label">Адрес проживания</label>
				<div class="controls">
					<input type="text" id="inputAddrreal" name="applicants[addrreal]" value="<?php echo CHtml::resolveValue($applicants,'addrreal') ?>" data-toggle="tooltip" title="<?php echo CHtml::encode($applicants->getError('addrreal')) ?>">
				</div>
			</div>
			<div class="control-group">
				<label for="inputAddrreg" class="control-label">Адрес прописки</label>
				<div class="controls">
					<input type="text" id="inputAddrreg" name="applicants[addrreg]" value="<?php echo CHtml::resolveValue($applicants,'addrreg') ?>" data-toggle="tooltip" title="<?php echo CHtml::encode($applicants->getError('addrreg')) ?>">
				</div>
			</div>
			<div class="control-group<?php if($applicants->hasErrors('phones')) echo $errorClass; ?>">
				<label for="inputPhones" class="control-label">Контактные телефоны<i title="Это поле необходимо заполнить"></i></label>
				<div class="controls">
					<input type="text" id="inputPhones" name="applicants[phones]" value="<?php echo CHtml::resolveValue($applicants,'phones') ?>" data-toggle="tooltip" title="<?php echo CHtml::encode($applicants->getError('phones')) ?>">
				</div>
			</div>
		</section>
		
		<section>
			<h4>Сведения об образовании:</h4>
			<table class="table table-bordered">
				<thead><tr>
					<th>Год поступления</th>
					<th>Год окончания</th>
					<th>Название учебного заведения, форма обучения</th>
					<th>Факультет</th>
					<th>Специальность</th>
					<th>Квалификация</th>
				</tr></thead>
				<tbody>
					<?php foreach($applicantEducation as $i=>$model): ?>
						<tr>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][yearstart]" style="width:63px;"><?php echo CHtml::resolveValue($model,'yearstart') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][yearend]" style="width:63px;"><?php echo CHtml::resolveValue($model,'yearend') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][name]" style="width:167px;"><?php echo CHtml::resolveValue($model,'name') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][faculty]" style="width:100px;"><?php echo CHtml::resolveValue($model,'faculty') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][speciality]" style="width:100px;"><?php echo CHtml::resolveValue($model,'speciality') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row" style="position:relative;">
									<textarea rows="2" name="applicantEducation[<?php echo $i ?>][qualification]" style="width:100px;"><?php echo CHtml::resolveValue($model,'qualification') ?></textarea>
									<?php if($i==(count($applicantEducation)-1) && $i!=0): ?><img src="<?php echo Yii::app()->homeUrl ?>img/icon-minus.png" class="delete-row" title="Удалить строку"><?php endif ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					<tr class="extra-row"<?php if(count($applicantEducation)==3) echo ' style="display:none;"' ?>>
						<td><div><div><span>Добавить строку</span></div></div></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</section>
		
		<section>
			<h4>Дополнительное образование (курсы, тренинги, повышение квалификации и т.п.):</h4>
			<table class="table table-bordered">
				<thead><tr>
					<th>Время учёбы</th>
					<th>Название курса, место учёбы</th>
				</tr></thead>
				<tbody>
					<?php foreach($applicantCourses as $i=>$model): ?>
						<tr>
							<td style="width:200px;">
								<div class="form-row">
									<textarea rows="2" name="applicantCourses[<?php echo $i ?>][timeperiod]"><?php echo CHtml::resolveValue($model,'timeperiod') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row" style="position:relative;">
									<textarea rows="2" name="applicantCourses[<?php echo $i ?>][description]" style="width:511px;"><?php echo CHtml::resolveValue($model,'description') ?></textarea>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					<tr class="extra-row"<?php if(count($applicantCourses)==3) echo ' style="display:none;"' ?>>
						<td><div><div><span>Добавить строку</span></div></div></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</section>
		
		<section>
			<h4>Профессиональная деятельность (включая работу без оформления и практику):</h4>
			<table class="table table-bordered">
				<thead><tr>
					<th>Название организации, <br>конт. телефон</th>
					<th>Период работы</th>
					<th>Должность</th>
					<th>Уровень оплаты</th>
					<th><strong>Должностные обязанности</strong></th>
					<th>Реальная причина увольнения</th>
				</tr></thead>
				<tbody>
					<?php foreach($applicantProfession as $i=>$model): ?>
						<tr>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][namedesc]" style="width:110px;"><?php echo CHtml::resolveValue($model,'namedesc') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][timeperiod]" style="width:60px;"><?php echo CHtml::resolveValue($model,'timeperiod') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][position]" style="width:83px;"><?php echo CHtml::resolveValue($model,'position') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][salary]" style="width:60px;"><?php echo CHtml::resolveValue($model,'salary') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][duties]" style="width:140px;"><?php echo CHtml::resolveValue($model,'duties') ?></textarea>
								</div>
							</td>
							<td>
								<div class="form-row" style="position:relative;">
									<textarea rows="2" name="applicantProfession[<?php echo $i ?>][reasondism]" style="width:140px;"><?php echo CHtml::resolveValue($model,'reasondism') ?></textarea>
									<?php if($i==(count($applicantProfession)-1) && $i!=0): ?><img src="<?php echo Yii::app()->homeUrl ?>img/icon-minus.png" class="delete-row" title="Удалить строку"><?php endif ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
					<tr class="extra-row"<?php if(count($applicantProfession)==3) echo ' style="display:none;"' ?>>
						<td><div><div><span>Добавить строку</span></div></div></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</section>
		
		<section>
			<h4>Дополнительные сведения:</h4>
			
			<p>Оцените себя по уровню развития нижеперечисленных личностных качеств, используя 10-бальную шкалу (0 – минимальный показатель)</p>
			<div id="selfratingSliders">
				<?php
					$selfratingLabels = array(
						'Коммуникабельность',
						'Грамотная речь',
						'Клиентоориентированность',
						'Обучаемость',
						'Стрессоустойчивость'
					);
				?>
				<?php for($i==0;$i<count($selfratingLabels);$i++): ?>
				<div class="slider-group clearfix">
					<label for="inputSelfrating<?php echo $i+1 ?>"><?php echo $selfratingLabels[$i] ?></label>
					<div class="slider-value">&nbsp;<div class="slider-value-inner">0</div></div>
					<div class="slider-container">
						<?php
							$selratingLabel = 'selfrating'.($i+1);
							echo CHtml::dropDownList(
								'applicants['.$selratingLabel.']',
								$applicants->$selratingLabel,
								array('0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'),
								array('id'=>'inputSelfrating'.($i+1),'class'=>'selfrating-select')
							);
						?>
					</div>
				</div>
				<?php endfor; ?>
			</div>
		</section>
		
		<section class="wide">
			<div class="control-group">
				<label class="control-label" for="inputPc">Уровень владения ПК (с указанием программ)</label>
				<div class="controls">
					<textarea id="inputPc" name="applicants[pc]" rows="3"><?php echo CHtml::resolveValue($applicants,'pc') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputDriving">Водительские права</label>
				<div class="controls">
					<label for="inputDriving" class="checkbox inline">
						<input type="checkbox" id="inputDriving" name="applicants[driving]" value="1"<?php if(isset($applicants->driving)) echo ' checked' ?>> Есть
					</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputCar">Наличие автомобиля</label>
				<div class="controls">
					<label for="inputCar" class="checkbox inline">
						<input type="checkbox" id="inputCar" name="applicants[car]" value="1"<?php if(isset($applicants->car)) echo ' checked' ?>> Есть
					</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputLang">Знание иностранных языков, степень владения <br>(со словарём, читаю и могу объясняться, свободно)</label>
				<div class="controls">
					<textarea rows="3" id="inputLang" name="applicants[lang]"><?php echo CHtml::resolveValue($applicants,'lang') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputFamily">Семейное положение</label>
				<div class="controls">
					<input type="text" id="inputFamily" name="applicants[family]" value="<?php echo CHtml::resolveValue($applicants,'family') ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputChildren">Дети</label>
				<div class="controls">
					<label for="inputChildren" class="checkbox inline">
						<input type="checkbox" id="inputChildren" name="applicants[children]" value="1"<?php if(isset($applicants->children)) echo ' checked' ?>> Есть
					</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputHobby">Увлечения (хобби)</label>
				<div class="controls">
					<textarea rows="3" id="inputHobby" name="applicants[hobby]"><?php echo CHtml::resolveValue($applicants,'hobby') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputMedrestr">Медицинские ограничения</label>
				<div class="controls">
					<textarea rows="3" id="inputMedrestr" name="applicants[medrestr]"><?php echo CHtml::resolveValue($applicants,'medrestr') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputCredit">Наличие долгов, кредитов, неоплаченных счетов, выплата алиментов</label>
				<div class="controls">
					<textarea rows="3" id="inputCredit" name="applicants[credit]"><?php echo CHtml::resolveValue($applicants,'credit') ?></textarea>
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label" for="inputOutlaw">Была ли у Вас судимость, привлекались ли Вы в качестве подозреваемого, имелись ли административные нарушения, проблемы криминального характера</label>
				<div class="controls">
					<textarea rows="3" id="inputOutlaw" name="applicants[outlaw]"><?php echo CHtml::resolveValue($applicants,'outlaw') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputSalarywish">Желаемый уровень заработной платы</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" id="inputSalarywish" name="applicants[salarywish]" value="<?php echo CHtml::resolveValue($applicants,'salarywish') ?>">
						<span class="add-on rubl" style="font-size:14px" title="В рублях">q</span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputSkills">Ключевые знания и навыки</label>
				<div class="controls">
					<textarea rows="3" id="inputSkills" name="applicants[skills]"><?php echo CHtml::resolveValue($applicants,'skills') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputAchievements">Профессиональные достижения, поощрения и награды</label>
				<div class="controls">
					<textarea rows="3" id="inputAchievements" name="applicants[achievements]"><?php echo CHtml::resolveValue($applicants,'achievements') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputReasonprof">Причины выбора данной должности</label>
				<div class="controls">
					<textarea rows="3" id="inputReasonprof" name="applicants[reasonprof]"><?php echo CHtml::resolveValue($applicants,'reasonprof') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputReadylongwork">Готовность к ненормированному рабочему дню</label>
				<div class="controls">
					<label for="inputReadylongwork" class="checkbox inline">
						<input type="checkbox" id="inputReadylongwork" name="applicants[readylongwork]" value="1"<?php if(isset($applicants->readylongwork)) echo ' checked' ?>> Готов(а)
					</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputReadytrip">Готовность к командировкам (продолжительность, интенсивность)</label>
				<div class="controls">
					<textarea rows="3" id="inputReadytrip" name="applicants[readytrip]"><?php echo CHtml::resolveValue($applicants,'readytrip'); ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputDatework">Дата, с которой Вы можете приступить к работе</label>
				<div class="controls" style="white-space:nowrap;">
					<input type="text" id="inputDatework" name="applicants[datework]" value="<?php echo CHtml::resolveValue($applicants,'datework') ?>">
					<span style="position:relative;"><i class="icon-calendar" style="position:absolute; left:-21px; top:0; opacity:.7;" title="Ввод с помощью календаря"></i></span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputArmy">Отношение к воинской службе</label>
				<div class="controls">
					<textarea rows="3" id="inputArmy" name="applicants[army]"><?php echo CHtml::resolveValue($applicants,'army') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputBadhabits">Вредные привычки, отношение к спиртному, азартным играм</label>
				<div class="controls">
					<textarea rows="3" id="inputBadhabits" name="applicants[badhabits]"><?php echo CHtml::resolveValue($applicants,'badhabits') ?></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Паспорт</label>
				<div class="controls form-inline">
					<label for="inputPasspseries">Серия</label>
					<input style="width:100px;" type="text" id="inputPasspseries" name="applicants[passpseries]" value="<?php echo CHtml::resolveValue($applicants,'passpseries') ?>">
					&nbsp;&nbsp;<label for="inputPasspnum">№</label>
					<input style="width:100px;" type="text" id="inputPasspnum" name="applicants[passpnum]" value="<?php echo CHtml::resolveValue($applicants,'passpnum') ?>">
					<div style="display:block; margin-top:15px;">
						<label for="inputPasspgivenby">Кем выдан</label>
						<textarea rows="3" id="inputPasspgivenby" name="applicants[passpgivenby]"><?php echo CHtml::resolveValue($applicants,'passpgivenby') ?></textarea>
					</div>
				</div>
			</div>
		</section>
		
		<div style="text-align:center;"><button type="submit" class="btn btn-large btn-success">Отправить</button></div>
		
	</form>
</div>