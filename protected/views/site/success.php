<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->homeUrl.'css/front.css');
?>
<div style="padding:30px; margin:50px auto; width:400px; border-radius:15px; background:#fff; box-shadow:rgba(0,0,0,.3) 0 0 8px;">
	<div class="alert alert-success">
		<?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
	<p style="margin-bottom:0; text-align:center;">
		<a href="<?php echo Yii::app()->homeUrl ?>" class="btn btn-large btn-success">Ок</a>
	</p>
</div>
