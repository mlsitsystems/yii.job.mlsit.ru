<?php
	$homeUrl = Yii::app()->homeUrl;
	$basePath = Yii::app()->basePath;
	Yii::app()->clientScript->registerScript('homeUrlVar','var homeUrl=\''.Yii::app()->homeUrl.'\';',CClientScript::POS_HEAD)
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<title><?php echo Yii::app()->name ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="<?php echo $homeUrl ?>libs/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $homeUrl ?>libs/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.min.css">
		<link rel="stylesheet" href="<?php echo $homeUrl ?>css/main.css">
		<link rel="shortcut icon" href="<?php echo $homeUrl ?>favicon.ico" type="image/x-icon">
		<!--[if lt IE 9]>
		<script src="<?php echo $homeUrl ?>js/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<?php echo $content ?>
		<script src="<?php echo $homeUrl ?>libs/jquery/jquery.min.js"></script>
		<script src="<?php echo $homeUrl ?>libs/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?php echo $homeUrl ?>libs/jquery-ui/js/jquery.ui.datepicker-ru.js"></script>
		<script src="<?php echo $homeUrl ?>libs/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $homeUrl ?>js/main.js"></script>
	</body>
</html>
