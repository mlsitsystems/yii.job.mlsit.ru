<?php

/**
 * This is the model class for table "{{applicants}}".
 *
 * The followings are the available columns in table '{{applicants}}':
 * @property integer $id
 * @property integer $vacancy
 * @property string $fullname
 * @property string $datebirth
 * @property string $addrreal
 * @property string $addrreg
 * @property string $phones
 * @property integer $selfrating1
 * @property integer $selfrating2
 * @property integer $selfrating3
 * @property integer $selfrating4
 * @property integer $selfrating5
 * @property string $pc
 * @property string $driving
 * @property integer $car
 * @property string $lang
 * @property string $family
 * @property integer $children
 * @property string $hobby
 * @property string $medrestr
 * @property string $credit
 * @property string $outlaw
 * @property string $salarywish
 * @property string $skills
 * @property string $achievements
 * @property string $reasonprof
 * @property integer $readylongwork
 * @property integer $readytrip
 * @property string $army
 * @property string $badhabits
 * @property integer $passpseries
 * @property integer $passpnum
 * @property string $passpgivenby
 *
 * The followings are the available model relations:
 * @property ApplicantCourses[] $applicantCourses
 * @property ApplicantEducation[] $applicantEducations
 */
class Applicants extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{applicants}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vacancy, fullname, phones', 'required','message'=>'Это поле необходимо заполнить'),
			array('vacancy, selfrating1, selfrating2, selfrating3, selfrating4, selfrating5, car, children, readylongwork, passpseries, passpnum', 'numerical', 'integerOnly'=>true),
			array('datebirth, addrreal, addrreg, pc, driving, lang, family, hobby, medrestr, credit, outlaw, salarywish, skills, achievements, reasonprof, readytrip, datework, army, badhabits, passpgivenby', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vacancy, fullname, datebirth, addrreal, addrreg, phones, selfrating1, selfrating2, selfrating3, selfrating4, selfrating5, pc, driving, car, lang, family, children, hobby, medrestr, credit, outlaw, salarywish, skills, achievements, reasonprof, readylongwork, readytrip, datework, army, badhabits, passpseries, passpnum, passpgivenby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applicant' 			=> array(self::BELONGS_TO, 'Vacancies', 'vacancy'),
			'applicantCourses' 		=> array(self::HAS_MANY, 'ApplicantCourses', 	'applicant_id'),
			'applicantEducations'	=> array(self::HAS_MANY, 'ApplicantEducation', 	'applicant_id'),
			'applicantProfessions' 	=> array(self::HAS_MANY, 'ApplicantProfession', 'applicant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'vacancy' => 'Вакансия',
			'fullname' => 'ФИО',
			'datebirth' => 'Дата рождения',
			'addrreal' => 'Адрес проживания',
			'addrreg' => 'Адрес прописки',
			'phones' => 'Контактные телефоны',
			'selfrating1' => 'Коммуникабельность',
			'selfrating2' => 'Грамотная речь',
			'selfrating3' => 'Клиентоориентированность',
			'selfrating4' => 'Обучаемость',
			'selfrating5' => 'Стрессоустойчивость',
			'pc' => 'Уровень владения ПК',
			'driving' => 'Водительские права',
			'car' => 'Наличие автомобиля',
			'lang' => 'Иностранные языки',
			'family' => 'Семейное положение',
			'children' => 'Дети',
			'hobby' => 'Увлечения',
			'medrestr' => 'Мед. ограничения',
			'credit' => 'Долги, алименты',
			'outlaw' => 'Судимость',
			'salarywish' => 'Желаемая зарплата',
			'skills' => 'Ключевые знания и навыки',
			'achievements' => 'Проф. достижения',
			'reasonprof' => 'Причина выбора вакансии',
			'readylongwork' => 'Готовность к ненорм. раб. дню',
			'readytrip' => 'Готовность к командировкам',
			'datework' => 'Когда может выйти на работу',
			'army' => 'Воинская служба',
			'badhabits' => 'Вредные привычки',
			'passpseries' => 'Серия паспорта',
			'passpnum' => 'Номер паспорта',
			'passpgivenby' => 'Паспорт выдан',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vacancy',$this->vacancy);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('datebirth',$this->datebirth,true);
		$criteria->compare('addrreal',$this->addrreal,true);
		$criteria->compare('addrreg',$this->addrreg,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('selfrating1',$this->selfrating1);
		$criteria->compare('selfrating2',$this->selfrating2);
		$criteria->compare('selfrating3',$this->selfrating3);
		$criteria->compare('selfrating4',$this->selfrating4);
		$criteria->compare('selfrating5',$this->selfrating5);
		$criteria->compare('pc',$this->pc,true);
		$criteria->compare('driving',$this->driving,true);
		$criteria->compare('car',$this->car);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('family',$this->family,true);
		$criteria->compare('children',$this->children);
		$criteria->compare('hobby',$this->hobby,true);
		$criteria->compare('medrestr',$this->medrestr,true);
		$criteria->compare('credit',$this->credit,true);
		$criteria->compare('outlaw',$this->outlaw,true);
		$criteria->compare('salarywish',$this->salarywish,true);
		$criteria->compare('skills',$this->skills,true);
		$criteria->compare('achievements',$this->achievements,true);
		$criteria->compare('reasonprof',$this->reasonprof,true);
		$criteria->compare('readylongwork',$this->readylongwork);
		$criteria->compare('readytrip',$this->readytrip,true);
		$criteria->compare('datework',$this->datework,true);
		$criteria->compare('army',$this->army,true);
		$criteria->compare('badhabits',$this->badhabits,true);
		$criteria->compare('passpseries',$this->passpseries);
		$criteria->compare('passpnum',$this->passpnum);
		$criteria->compare('passpgivenby',$this->passpgivenby,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Applicants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
