<?php

/**
 * This is the model class for table "{{applicant_profession}}".
 *
 * The followings are the available columns in table '{{applicant_profession}}':
 * @property integer $id
 * @property integer $applicant_id
 * @property string $namedesc
 * @property string $timeperiod
 * @property string $position
 * @property string $salary
 * @property string $duties
 * @property string $reasondism
 *
 * The followings are the available model relations:
 * @property Applicants $applicant
 */
class ApplicantProfession extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ApplicantProfession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{applicant_profession}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('applicant_id', 'numerical', 'integerOnly'=>true),
			array('namedesc, timeperiod, position, salary, duties, reasondism', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, applicant_id, namedesc, timeperiod, position, salary, duties, reasondism', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applicant' => array(self::BELONGS_TO, 'Applicants', 'applicant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'applicant_id' => 'Номер резюме',
			'namedesc' => 'Название организации, контакты',
			'timeperiod' => 'Период работы',
			'position' => 'Должность',
			'salary' => 'Уровень оплаты',
			'duties' => 'Должностные обязанности',
			'reasondism' => 'Причина увольнения',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('applicant_id',$this->applicant_id);
		$criteria->compare('namedesc',$this->namedesc,true);
		$criteria->compare('timeperiod',$this->timeperiod,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('salary',$this->salary,true);
		$criteria->compare('duties',$this->duties,true);
		$criteria->compare('reasondism',$this->reasondism,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}