<?php

class SiteController extends CController
{
	public $breadcrumbs=array();
	
	public function filters()
	{
		return array(
			array(
				'HttpAuthFilter + login',
				'realm'=>'',
			),
		);
	}

	public function actionLogin()
	{
		$user=Yii::app()->user;
		if($user->isGuest) {
			$identity=new UserIdentity('','');
			$identity->authenticate();
			$user->login($identity,0);
			$this->redirect($user->returnUrl);
		} else
			$this->redirect(Yii::app()->homeUrl);
	}

	public function actionIndex()
	{
		$applicants = new Applicants;
		$applEduModels = array(new ApplicantEducation);
		$applCrsModels = array(new ApplicantCourses);
		$applPrfModels = array(new ApplicantProfession);
		$applEduValid = true;
		$applCrsValid = true;
		$applPrfValid = true;

		if(isset($_POST['applicants']))
		{
			$applicants->attributes=$_POST['applicants'];
			
			$applEduModels = array();
			foreach($_POST['applicantEducation'] as $i=>$values) {
				$applEduModels[$i] = new ApplicantEducation;
				$applEduModels[$i]->attributes = $_POST['applicantEducation'][$i];
				$applEduValid = $applEduModels[$i]->validate() && $applEduValid;
			}
			
			$applCrsModels = array();
			foreach($_POST['applicantCourses'] as $i=>$values) {
				$applCrsModels[$i] = new ApplicantCourses;
				$applCrsModels[$i]->attributes = $_POST['applicantCourses'][$i];
				$applCrsValid = $applCrsModels[$i]->validate() && $applCrsValid;
			}
			
			$applPrfModels = array();
			foreach($_POST['applicantProfession'] as $i=>$values) {
				$applPrfModels[$i] = new ApplicantProfession;
				$applPrfModels[$i]->attributes = $_POST['applicantProfession'][$i];
				$applPrfValid = $applPrfModels[$i]->validate() && $applPrfValid;
			}
			
			if($applicants->validate() && $applEduValid && $applCrsValid && $applPrfValid) {
				Yii::app()->user->setFlash('success', '<strong>Отлично!</strong> Мы свяжемся с Вами в ближайшее время.');
				Yii::app()->user->setState('submitSuccess',1);
				if($applicants->save(false)) {
					foreach($applEduModels as $i=>$model) {
						$model->applicant_id = $applicants->id;
						$vls=join('',$_POST['applicantEducation'][$i]);
						if(!empty($vls))
							$model->save(false);
					}
					foreach($applCrsModels as $i=>$model) {
						$model->applicant_id = $applicants->id;
						$vls=join('',$_POST['applicantCourses'][$i]);
						if(!empty($vls))
							$model->save(false);
					}
					foreach($applPrfModels as $i=>$model) {
						$model->applicant_id = $applicants->id;
						$vls=join('',$_POST['applicantProfession'][$i]);
						if(!empty($vls))
							$model->save(false);
					}
					$this->sendEmails($applicants);
					$this->redirect(array('success'));
				}
			}
		}
		if (Vacancies::model()->count())
			$this->render('form',array(
				'applicants'=>$applicants,
				'applicantEducation'=>$applEduModels,
				'applicantEducationValid'=>$applEduValid,
				'applicantCourses'=>$applCrsModels,
				'applicantCoursesValid'=>$applCrsValid,
				'applicantProfession'=>$applPrfModels,
				'applicantProfessionValid'=>$applPrfValid,
			));
		else
			$this->renderText('Сначала нужно добавить хоть одну вакансию. <a href="'.$this->createUrl('/admin/default/vacancies',array('do'=>'create')).'">Добавить</a>');
	}
	
	public function actionSuccess()
	{
		if(Yii::app()->user->getState('submitSuccess')) {
			$this->render('success');
			Yii::app()->user->setState('submitSuccess',0);
		} else
			throw new CHttpException(404);
	}
	
	public function actionTest() {
		$model = new Applicants;
		$applicant = $model->findByPk(2);
		$this->render('test',array('applicant'=>$applicant));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function sendEmails($applicants) {
		$to = 'info@mlsit.ru';
		$subject = "=?utf-8?B?" . base64_encode("Заполнена форма на job.mlsit.ru") . "?=";
		$headers  = 'From: job@mlsit.ru' . "\r\n";
		//$headers .= 'Reply-To: ' . "\r\n";
		$headers .= 'BCC: egor.malygin@gmail.com, vinogradov@iiweb.ru' . "\r\n";//скрытая копия
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
		$message = '<html><body bgcolor="#ffffff">';
		$message .= $this->widget('zii.widgets.CDetailView',array(
			'data'=>$applicants,
			'htmlOptions'=>array(
				'border'=>'1',
			),
			//'baseScriptUrl'=>'/',
		),true);
		$message .= '</body></html>';
		@mail($to, $subject, $message, $headers);
	}
	

	/**
	 * Performs the AJAX validation.
	 * @param Applicant $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='applicant-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
